from typing import Any, Callable, Optional, List

from Trees.TreeNode import TreeNode


class BinaryNode(TreeNode):

    # if there is a way to add variable
    # that is a reference to a list member pls tell me
    # I know it is not how I should do this
    # but I wanted to mess around with python inheritance

    def left_child(self) -> 'BinaryNode':
        return self.children[0]

    def right_child(self) -> 'BinaryNode':
        return self.children[1]
    
    def add(self) -> None:
        return None

    def min(self) -> 'BinaryNode':
        if self.children[0].value > self.children[1].value:
            return self.children[1]
        else:
            return self.children[0]

    def add_left_child(self, value: Any) -> None:
        self.children[0] = BinaryNode(value)

    def add_right_child(self, value: Any) -> None:
        self.children[1] = BinaryNode(value)

    def is_leaf(self) -> bool:
        if (self.children[1] is None) and (self.children[0] is None):
            return True
        else:
            return False

    def traverse_in_order(self, visit: Callable[[Any], None]) -> None:
        if self.children[0] is not None:
            self.children[0].traverse_in_order(visit)
        visit(self)
        if self.children[1] is not None:
            self.children[1].traverse_in_order(visit)

    def traverse_post_order(self, visit: Callable[[Any], None]) -> None:
        if self.children[0] is not None:
            self.children[0].traverse_post_order(visit)
        if self.children[1] is not None:
            self.children[1].traverse_post_order(visit)
        visit(self)

    def traverse_pre_order(self, visit: Callable[[Any], None]) -> None:
        visit(self)
        if self.children[0] is not None:
            self.children[0].traverse_pre_order(visit)
        if self.children[1] is not None:
            self.children[1].traverse_pre_order(visit)

    def __init__(self, value: Optional=None):
        super().__init__(value)
        self.children.append(None)
        self.children.append(None)


