from typing import Any, Callable, Optional, List, Tuple, Union
import matplotlib.pyplot as plt
from BinaryNode import BinaryNode
from Trees.Tree import Tree


class BinaryTree(Tree):

    def traverse_in_order(self, visit: Callable[[Any], None]) -> None:
        self.root.traverse_in_order(visit)

    def traverse_post_order(self, visit: Callable[[Any], None]) -> None:
        self.root.traverse_post_order(visit)

    def traverse_pre_order(self, visit: Callable[[Any], None]) -> None:
        self.root.traverse_pre_order(visit)

    def __init__(self, value: Optional = None, root: BinaryNode = None) -> None:
        super().__init__(value)
        if root is None:
            self.root = BinaryNode(value)
        else:
            self.root = root


x: BinaryNode = BinaryNode(1)
x.add_left_child(2)
x.add_right_child(3)
x.children[0].add_left_child(4)
x.children[0].add_right_child(5)
x.children[0].children[0].add_left_child(8)
x.children[0].children[0].add_right_child(9)
x.children[1].add_right_child(7)

tree: BinaryTree = BinaryTree(root=x)
tree.show()
#
# assert tree.root.value == 10
# assert tree.root.children[1].value == 2
# assert tree.root.children[1].is_leaf() is False
# assert tree.root.children[0].children[0].value == 1
# assert tree.root.children[0].children[0].is_leaf() is True


def horizontal_sum(tree: BinaryTree) -> List[Any]:
    result: List[Any] = []

    def level_assignment(node: 'TreeNode', level: int = 0) -> None:
        if len(result) <= level:
            result.append(node.value)
        else:
            result[level] = result[level] + node.value
        for i in node.children[0], node.children[1]:
            if i is not None:
                level_assignment(i, level + 1)

    level_assignment(tree.root)
    return result


test_list = horizontal_sum(tree)
print(test_list)
tree.show()
